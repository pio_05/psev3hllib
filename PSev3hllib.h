#ifndef __PSev3hllib__
#define __PSev3hllib__

#include <stdio.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/time.h>

#include "lms2012.h"
#include "lmstypes.h"



#define motorA 0x01
#define motorB 0x02
#define motorC 0x04
#define motorD 0x08

int SetMotorPower(char MOTOR,int MOTOR_POWER); //ustawia moc silnikow
                                               //motorA+motorD dozwolone

int ReadEncoder(char MOTOR);   //czyta wartosc enkodera podanego silnika
                               //mozna potac jeden silnik

int ResetEncoder(char MOTOR);  //ustawia wartosc enkodera na 0
                               //efektem ubocznym jest ustawienie mocy na 0
                               //mozna kozystac motorA+motorB

typedef enum{ERROR=-1,NONE, UP, CENTER, DOWN, RIGHT, LEFT, BACK} Button;

Button ButtonPressed();

#define Wait sleep     //definiujemy Wait jako sleep(czekanie 1 sekundy)
                       //zeby wszystko ladnie wygladalo

void Waitms(int TIME); //czeka przez ilosc milisekund podana jako
                       //paranetr Waitms(1000) czeka 1 sekunde

int PlaySound(int frequency, int duration, char volume);
                       //odtwarza dzwiek o czestotliwosci frequency,
                       //czasie trwania duration i glosnosci volume
                       //glosnos w przedziale 0..100
                       
int Beep();            //prosty sposob wydania dzwieku

typedef struct{
  enum {SNONE = 0, STOUCH = 0, SGYRO, SCOLOR, SAMBIENT, SREFLECT, SSONAR} Type;
  enum {P1, P2, P3, P4} Port;
}Sensor;

int SensorsInit(Sensor *s1, Sensor *s2, Sensor *s3, Sensor *s4);
                                         //Przypisuje odpowiedni mod do sensora

long SensorMeasure(Sensor sensor);        //Pobiera sructure sensora jako
                                         //parametr, nastepnie wykonuje
                                         //pomiar

#define SensorMeasure SensorRead         //Measure to brzydkie slowo

int LcdPrintA(char *pText);              //wypisuje tablice znakow char
                                         //zakonczana znakiem 0

int LcdPrintI(int i);                    //wypisuje liczbe staloprzecinkowa

int LcdPrintF(double f);                 //wypisuje liczbe zmiennoprzecinkowa

int CountTime();     //zwraca czas w milisekundach od poprzedniego wywolania
                     //funkcji

#endif
